# Actor Haskell

My own attempt at making a library to support Erlang style (actor model) concurrency in Haskell

### Example Program

A basic concurrent program consisting of two spawned processes that message each other back and forth a fixed number of times before finishing.
Based on [this program](http://erlang.org/doc/getting_started/conc_prog.html#message-passing).

```haskell
import Control.Concurrent.Process

data Ping
  = Ping (ProcessId Pong)
  | Finished

data Pong = Pong

ping :: Integer -> ProcessId Ping -> Process Pong ()
ping 0 pongId = do
  pongId ! Finished
  liftIO $ putStrLn "Ping finished"
ping numLeft pongId = do
  pingId <- self
  pongId ! Ping pingId
  Pong <- receive
  liftIO $ putStrLn "Ping received pong"
  ping (numLeft - 1) pongId

pong :: Process Ping ()
pong = do
  msg <- receive
  case msg of
    Finished      -> liftIO $ putStrLn "Pong finished"
    Ping pingId -> do
      liftIO $ putStrLn "Pong received ping"
      pingId ! Pong
      pong

start :: IO ()
start = runProcess $ do
  pongId <- spawn pong
  void $ spawn $ ping 3 pongId
```
