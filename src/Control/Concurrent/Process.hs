module Control.Concurrent.Process
  ( ProcessId()
  , Process()
  , spawn
  , (!)
  , receive
  , self
  , runProcess
  , spawnWithSelf
  , module Control.Monad
  , module Control.Monad.IO.Class
  ) where

import Control.Applicative
import Control.Concurrent
import Control.Concurrent.STM
import Control.Monad

import Control.Monad.IO.Class
import Control.Monad.Trans.Reader

data ProcessData a = ProcessData
  { processId :: ProcessId a
  }

-- | This type is used store the info needed to send messages to the
--   process it is associated with.
--   The type parameter represents what type can be sent to the process.
newtype ProcessId a = ProcessId { messageQueue :: TQueue a }

-- | Values of this type represent the computations of a single process, which can
--   include recalling the current process's 'ProcessId', and creating and sending messages
--   to processes, as well as any 'IO' computation through the use of 'liftIO'.
--
--   The first type parameter represents the type that messages sent to the process must be,
--   while the second type parameter represents the type of the result of the computation.
newtype Process a b = Process { unpackProcess :: ReaderT (ProcessData a) IO b }

instance Functor (Process a) where
  fmap f (Process m) = Process $ fmap f m

instance Applicative (Process a) where
  pure x = Process $ pure x
  liftA2 f (Process a) (Process b) = Process $ liftA2 f a b

instance Monad (Process a) where
  (Process m) >>= f = Process $ m >>= unpackProcess . f

instance MonadIO (Process a) where
  liftIO m = Process $ liftIO m

-- | A computation which, given a process, spawns that process on a new thread,
--   and produces a 'ProcessId' as a result which can be used to send messages to
--   the new process.
spawn :: Process a ()            -- ^ The process to be run
      -> Process b (ProcessId a) -- ^ The 'ProcessId' of the now newly initialized process
spawn newProc = liftIO $ do
  msgQueue <- atomically newTQueue
  forkIO $ runReaderT (unpackProcess newProc) $ ProcessData $ ProcessId msgQueue
  return $ ProcessId msgQueue

-- | A convenience function for creating a process that needs to send messages to the
--   current process and thus needs the current processes's 'ProcessId'.
--
--   See 'Control.Concurrent.Process.spawn'.
spawnWithSelf :: (ProcessId b -> Process a ()) -> Process b (ProcessId a)
spawnWithSelf newProc = self >>= spawn . newProc

-- | An operator to send a message to the process associated with the given 'ProcessId'.
--   The message is type checked against the 'ProcessId' to insure that the process is
--   receiving the correct type of message.
(!) :: ProcessId a -- ^ The ID of the process the message will be sent to
    -> a           -- ^ The message
    -> Process b ()
pid ! msg = liftIO $ atomically $ writeTQueue (messageQueue pid) msg

-- | Retrieves the oldest message in the current process's message queue
--   or blocks until the process receives a message.
receive :: Process a a
receive = Process $ ask >>= liftIO . atomically . readTQueue . messageQueue . processId

-- | Gets the 'ProcessId' of the current process.
self :: Process a (ProcessId a)
self = Process $ processId <$> ask

-- | Initializes and runs the given process /in the same thread/, turning the 'Process'
--   computation into an 'IO' computation.
runProcess :: Process a () -- ^ The process computation to run
           -> IO ()
runProcess proc = atomically newTQueue >>= runReaderT (unpackProcess proc) . ProcessData . ProcessId
